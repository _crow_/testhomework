import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class Test1 {
    public WebDriver driver;
    public WebElement element;
    public boolean result;

    @BeforeClass
    void before(){
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(priority = 1)
    void Precondition(){
        driver.get("https://demo.opencart.com/index.php?route=product/product&product_id=40");
        element = driver.findElement(By.xpath("//a[text()='Your Store']"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Logo is a not displayed");
    }

    @Test(priority = 2)
    void ClickBuyButton(){
        element = driver.findElement(By.xpath("//button[text()='Add to Cart']"));
        element.click();
    }

    @Test(priority = 3)
    void OneItemInCart() throws InterruptedException {
        boolean actual = false;
        for(int i=0;i<10;i++){
            element = driver.findElement(By.xpath("//span[@id='cart-total']"));
            actual = element.getText().contains("1 item(s)");
            if(actual){break;}else {
                Thread.sleep(50); //TODO sleep don`t use
            }
        }
        Assert.assertTrue(actual, "Cart does not contain 1 item");
    }

    @Test(priority = 4)
    void ClickToBrand(){
    element=driver.findElement(By.xpath("//a[text()='Apple']"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Brand is a not displayed");
        element.click();
    }
    @Test(priority = 5)
    void AddToCart() throws InterruptedException {
        element = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='iPhone']]//following-sibling::div//span[.='Add to Cart']"));
        result = element.isDisplayed();
        Assert.assertTrue(result, "Button is a not displayed");
        element.click();
        Thread.sleep(1000); //TODO sleep don`t use
    }
    @Test(priority = 6)
    void MessageIsOnWindow(){
        element=driver.findElement(By.xpath("//div/a[.='iPhone']"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Message is a not displayed");
    }

    @Test(priority = 7)
    void ClickCart(){
        element=driver.findElement(By.xpath("//div[@id='cart']/button"));
        result=element.isDisplayed();
        Assert.assertTrue(result,"Cart is a not displayed");
        element.click();
    }
    @Test(priority = 8)
    void ClickViewCart() throws InterruptedException {
        element=driver.findElement(By.xpath("//a/strong[.=' View Cart']"));
        element.click();
    }

    @AfterClass
    void postCondition(){
        driver.quit();
    }
}
